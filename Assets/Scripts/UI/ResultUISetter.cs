﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResultUISetter : Singleton<ResultUISetter>
{
    [SerializeField] private Text savedText;
    [SerializeField] private Text killsText;
    [SerializeField] private Text loopsText;
    [SerializeField] private Text finishText;

    [SerializeField] private GameObject resultPanel;
    public GameObject ResultPanel => resultPanel;

    private void Awake()
    {
        Score.Instance.GaysSaved.AddListener(ShowSavedGays);
        Score.Instance.BallonsKilled.AddListener(ShowKilledEnemies);
        Score.Instance.LoopsFinished.AddListener(ShowFinishedLoops);
        Score.Instance.BossKilled.AddListener(ShowWhetheBossIsKilled);
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }

    private void ShowSavedGays(int saved)
    {
        savedText.text = "Gays saved:" + "\r\n" + saved.ToString();
    }

    private void ShowKilledEnemies(int kills)
    {
        killsText.text = "Balloons killed:" + "\r\n" + kills.ToString();
    }

    private void ShowFinishedLoops(int loops)
    {
        loopsText.text = "Loops completed:" + "\r\n" + loops.ToString();
    }

    private void ShowWhetheBossIsKilled(bool finish)
    {
        if (!finish)
        {
            finishText.text = "Is boss killed?" + "\r\n" + "YOU FAILED";
        }
        else
        {
            finishText.text = "Result:" + "\r\n" + "YOU WIN";
        }
    }
}