﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthView : Singleton<HealthView>
{
    [SerializeField] private Slider _slider;

    private void Awake()
    {
        Health.Instance.Points.AddListener(SetView);
    }

    public void SetView(float value)
    {
        _slider.value = value / 100;
    }
}
