﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            GetComponent<Animator>().SetBool("IsAttack", true);
        }
    }

    public void StopAttackAnim()
    {
        GetComponent<Animator>().SetBool("IsAttack", false);
    }

    public void PlaySwordAttackSound()
    {
        Audio.Instance.PlaySound("SwordAttack");
    }
}