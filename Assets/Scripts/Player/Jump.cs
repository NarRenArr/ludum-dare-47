﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    [SerializeField]
    [Range(0, 20)]
    private float _force;

    [SerializeField]
    [Range(0, 20)]
    private float _staticGravityForce;

    [SerializeField] private KeyCode _key;

    private Animator _animator;

    private Vector3 _startPos;

    private bool _isDoubleJump;
    private bool _isJump;

    private float _jumpTimer;

    private void Awake()
    {
        _startPos = transform.position;
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        _animator.SetFloat("Height", transform.position.y - _startPos.y);

        if (transform.position.y > _startPos.y && GetComponent<Rigidbody2D>().velocity.y <= 7f)
        {
            transform.position -= new Vector3(0, _staticGravityForce * Time.deltaTime, 0);

            if (transform.position.y < _startPos.y)
            {
                transform.position = new Vector3(transform.position.x, _startPos.y, transform.position.z);
            }
        }
        else if (_isJump && transform.position.y <= _startPos.y && GetComponent<Rigidbody2D>().velocity.y <= 7f)
        {
            _isDoubleJump = false;
            _animator.SetBool("IsDoubleJump", _isDoubleJump);
            _isJump = false;
        }

        _animator.SetBool("IsDoubleJump", false);

        if (_jumpTimer > 0)
        {
            _jumpTimer -= Time.deltaTime;
        }
        else if (Input.GetKeyDown(_key) && !_isDoubleJump && !_animator.GetBool("IsSlide"))
        {
            if (transform.position.y > _startPos.y)
            {
                _isDoubleJump = true;
                _animator.SetBool("IsDoubleJump", _isDoubleJump);
            }

            DoJump();
        }

    }

    private void DoJump()
    {
        transform.position += new Vector3(0, 0.1f, 0);
        Audio.Instance.PlaySound("Jump");
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        GetComponent<Rigidbody2D>().AddForce(transform.up * _force * 100);
        _jumpTimer = 0.01f;
        _isJump = true;
    }
}
