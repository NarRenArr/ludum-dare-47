﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : Singleton<Health>
{
    [SerializeField] public GenericFlag<float> Points = new GenericFlag<float>(100);

    public bool IsZeroHealth
    {
        get
        {
            if (Points.Value <= 0)
                return true;
            else
                return false;
        }
    }

    private void Update()
    {
        //ApplyDamag(1 * Time.deltaTime);
    }

    public void ApplyDamag(float damage)
    {
        Points.Value -= damage;

        if (Points.Value < 0)
            Points.Value = 0;
    }

    public void ApplyHeal(float heal)
    {
        Points.Value += heal;

        if (Points.Value > 100)
            Points.Value = 100;
    }
}
