﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private GameObject _followingObject;

    private float _xOffset;
    private void Start()
    {
        _xOffset = _followingObject.transform.position.x;
    }

    void Update()
    {
        if (!ZaLoopo.IsZaLoopo)
            transform.position = new Vector3(_followingObject.transform.position.x - _xOffset, transform.position.y, transform.position.z);
    }
}
