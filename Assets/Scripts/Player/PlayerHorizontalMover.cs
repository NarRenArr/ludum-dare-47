﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHorizontalMover : HorizontalMover
{
    public void SetSpeedToSlow()
    {
        speedMultiplier = 1 / Speed * Villain.Instance.GetComponent<HorizontalMover>().Speed;
    }

    public void SetSpeedToNormal()
    {
        speedMultiplier = 1;
    }
}
