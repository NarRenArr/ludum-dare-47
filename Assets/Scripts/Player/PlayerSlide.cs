﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSlide : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            GetComponent<Animator>().SetBool("IsSlide", true);
        }
    }

    public void StopSlideAnim()
    {
        GetComponent<Animator>().SetBool("IsSlide", false);
    }

    public void PlaySlideSound()
    {
        Audio.Instance.PlaySound("Slide");
    }
}