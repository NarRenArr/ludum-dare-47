﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillainDeath : Singleton<VillainDeath>
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Sword")
        {
            foreach (Enemy enemy in FindObjectsOfType<Enemy>())
            {
                Destroy(enemy.gameObject);
            }
            Destroy(EnemyGenerator.Instance.gameObject);

            Cutscene.Instance.StartVillainDeath();
        }
    }
}
