﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Villain : Singleton<Villain>
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            collision.attachedRigidbody.GetComponent<PlayerHorizontalMover>().SetSpeedToSlow();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            collision.attachedRigidbody.GetComponent<PlayerHorizontalMover>().SetSpeedToNormal();
    }
}
