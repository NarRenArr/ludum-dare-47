﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZaLoopo : Singleton<ZaLoopo>
{
    private  static bool _isZaLoopo;
    public static bool IsZaLoopo => _isZaLoopo;

    [SerializeField] private GameObject _background1;
    [SerializeField] private GameObject _background2;
    [SerializeField] private GameObject _background3;
    [SerializeField] private GameObject _background4;

    private Vector3 _villainInitPos;
    private Vector3 _playerInitPos;

    private void Awake()
    {
        _villainInitPos = transform.position;
        _playerInitPos = Health.Instance.transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Sword")
        {
            foreach (Enemy enemy in FindObjectsOfType<Enemy>())
            {
                Destroy(enemy.gameObject);
            }

            Score.Instance.LoopsFinished.Value += 1;

            _isZaLoopo = true;

            Health.Instance.ApplyHeal(100);
            Cutscene.Instance.StartZaLoopo();
        }
    }

    public void SetObjectsToInitialCondition()
    {
        transform.position = _villainInitPos;
        Health.Instance.transform.position = _playerInitPos;
        _background1.transform.position = new Vector3(0, _background1.transform.position.y, _background1.transform.position.z);
        _background2.transform.position = new Vector3(0, _background2.transform.position.y, _background2.transform.position.z);
        _background3.transform.position = new Vector3(0, _background3.transform.position.y, _background3.transform.position.z);
        _background4.transform.position = new Vector3(0, _background4.transform.position.y, _background4.transform.position.z);

        _isZaLoopo = false;

        Cutscene.Instance.SwitchOffCutscenes();
    }
}
