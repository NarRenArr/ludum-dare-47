﻿using System;
using System.Linq;

[System.Serializable]
public class GenericFlag<T>
{
    private Action<T> OnValueChanged = (v) => { };
    private T _value;

    public T Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = value;
            OnValueChanged(_value);
        }
    }


    public GenericFlag(T value)
    {
        _value = value;
    }

    public void AddListener(Action<T> method)
    {
        OnValueChanged += method;
        method(Value);
    }

    public void RemoveListener(Action<T> method)
    {
        OnValueChanged -= method;
    }
}
