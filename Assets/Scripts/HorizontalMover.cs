﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalMover : MonoBehaviour
{
    [SerializeField]
    [Range(0, 15)]
    private float _speed;
    public float Speed => _speed;

    protected float speedMultiplier = 1;

    void FixedUpdate()
    {
        if (!ZaLoopo.IsZaLoopo)
            MoveForward();
    }

    private void MoveForward()
    {
        transform.position += new Vector3(_speed * speedMultiplier * Time.deltaTime, 0, 0);
    }
}
