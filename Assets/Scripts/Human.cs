﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : MonoBehaviour
{
    [SerializeField] private float _humadDeathDamage;

    private bool _isAfterPlayer;

    private void Update()
    {
        if (!_isAfterPlayer)
            if (Health.Instance.transform.position.x > transform.position.x + 0.3f)
            {
                _isAfterPlayer = true;
                Health.Instance.ApplyDamag(_humadDeathDamage);
                Audio.Instance.PlaySound("GetDamaged");
            }
    }
}
