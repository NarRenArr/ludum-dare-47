﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : Singleton<EnemyGenerator>
{
    [SerializeField] private GameObject _firstEnemyPrefab;
    [SerializeField] private GameObject _secondEnemyPrefab;
    [SerializeField] private GameObject _spawnPos;

    [Range(0f,10f)]
    [SerializeField] private float _enemyProportion;

    [SerializeField] private float _topBorder;
    [SerializeField] private float _bottomBorder;

    [SerializeField] private float _generationSpeed;

    private float _loopSpeedMultiplier = 1;
    private float _sessionSpeedMultiplier = 1;

    private void Awake()
    {
        Generator();
    }

    private void Update()
    {
        _sessionSpeedMultiplier += Time.deltaTime / 90;
        _loopSpeedMultiplier += Time.deltaTime / 20;
    }

    public void ResetLoop()
    {
        _loopSpeedMultiplier = 1;
    }

    public void Generator()
    {
        if (!ZaLoopo.IsZaLoopo)
        {
            float _offsetY = Random.Range(_bottomBorder, _topBorder);

            Vector3 pos = new Vector3(_spawnPos.transform.parent.position.x + _spawnPos.transform.position.x, _offsetY, transform.position.z);

            float _r = Random.Range(0f, 10f);

            if (_r > _enemyProportion)
            {
                Instantiate(_secondEnemyPrefab, pos, Quaternion.identity);
            }
            else if (_r < _enemyProportion)
            {

                Instantiate(_firstEnemyPrefab, pos, Quaternion.identity);
            }
        }

        Invoke("Generator", _generationSpeed / _sessionSpeedMultiplier / _loopSpeedMultiplier);
    }
}
