﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Victory : Singleton<Victory>
{
    public void StartVictory()
    {
        Audio.Instance.PlaySound("Victory");
        ResultUISetter.Instance.ResultPanel.SetActive(true);
        Score.Instance.BossKilled.Value = true;
        Time.timeScale = 0;
    }
}
