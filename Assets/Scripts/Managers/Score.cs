﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : Singleton<Score>
{
    public GenericFlag<int> GaysSaved = new GenericFlag<int>(0);
    public GenericFlag<int> BallonsKilled = new GenericFlag<int>(0);
    public GenericFlag<int> LoopsFinished = new GenericFlag<int>(0);
    public GenericFlag<bool> BossKilled = new GenericFlag<bool>(false);
}
