﻿using UnityEngine;
using System;

/// <summary>
/// Allows to controll every sonud.
/// </summary>
public class Audio : Singleton<Audio>
{
    public Sound[] sounds;

    private void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    private void Start()
    {
        PlaySound("Theme");
    }

    /// <summary>
    /// Plays a particular sound by it's name.
    /// </summary>
    /// <param name="name">The name of a sound.</param>
    /// <returns></returns>
    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
        }

        s.source.Play();
    }

    /// <summary>
    /// Returns the sound source of a sound by it's name.
    /// </summary>
    /// <param name="name">The name of a sound.</param>
    /// <returns></returns>
    public AudioSource GetSoundSource(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!");
        }

        return s.source;
    }

    [System.Serializable]
    public class Sound
    {
        public string name;

        public AudioClip clip;

        [Range(0f, 1f)] public float volume = 0.7f;
        [Range(1f, 3f)] public float pitch = 1;

        public bool loop;

        [HideInInspector]
        public AudioSource source;
    }
}
