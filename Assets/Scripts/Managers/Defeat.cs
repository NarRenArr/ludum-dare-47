﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defeat : Singleton<Defeat>
{
    private void Awake()
    {
        Health.Instance.Points.AddListener(StartDefeat);
    }

    public void StartDefeat(float health)
    {
        if (health <= 0)
        {
            Audio.Instance.PlaySound("Defeat");
            ResultUISetter.Instance.ResultPanel.SetActive(true);
            Score.Instance.BossKilled.Value = false;
            Time.timeScale = 0;
        }
    }
}
