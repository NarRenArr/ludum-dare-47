﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZaLoopoCut : MonoBehaviour
{
    public void SetObjectsToInitialCondition()
    {
        ZaLoopo.Instance.SetObjectsToInitialCondition();
    }

    public void PlayZaLoopoSound()
    {
        Audio.Instance.PlaySound("ZaLoopo");
    }
}
