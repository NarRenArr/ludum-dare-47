﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillainDeathCut : MonoBehaviour
{
    public void StartVictory()
    {
        Victory.Instance.StartVictory();
    }

    public void PlayOhNoSound()
    {
        Audio.Instance.PlaySound("OhNo");
    }
}
