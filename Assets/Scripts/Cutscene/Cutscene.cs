﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cutscene : Singleton<Cutscene>
{
    [SerializeField] private GameObject _villainDeath;
    public GameObject VillainDeath => _villainDeath;

    [SerializeField] private GameObject _zaLoopo;
    public GameObject ZaLoopo => _zaLoopo;

    public void StartVillainDeath()
    {
        SetCutsceneMode();
        VillainDeath.SetActive(true);
        ZaLoopo.SetActive(false);
        print("VillainDeath animation");
    }

    public void StartZaLoopo()
    {
        SetCutsceneMode();
        ZaLoopo.SetActive(true);
        VillainDeath.SetActive(false);
        print("ZaLoopo animation");
    }

    public void SwitchOffCutscenes()
    {
        _villainDeath.SetActive(false);
        _zaLoopo.SetActive(false);
    }

    private void SetCutsceneMode()
    {
        Health.Instance.transform.position += Vector3.up * 5000;
        Health.Instance.transform.position -= Vector3.right * 10;
        Villain.Instance.transform.position += Vector3.up * 5000;
        EnemyGenerator.Instance.ResetLoop();
    }
}
