﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : Enemy
{
    [SerializeField] private float _heal;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Sword")
        {
            Health.Instance.ApplyHeal(_heal);
            Score.Instance.GaysSaved.Value += 1;
            Audio.Instance.PlaySound("Gratitude");
        }
        else if (collision.tag == "Player")
        {
            Health.Instance.ApplyDamag(_damage);
            Score.Instance.BallonsKilled.Value += 1;
            Audio.Instance.PlaySound("GetDamaged");
            Destroy(gameObject);
        }
    }
}
