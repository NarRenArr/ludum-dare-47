﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] protected float _damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Health.Instance.ApplyDamag(_damage);
            Score.Instance.BallonsKilled.Value += 1;
            Audio.Instance.PlaySound("GetDamaged");
            Destroy(gameObject);
        }
    }
}
