﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyFaceChanger : MonoBehaviour
{
    [SerializeField] private Sprite[] _faces;
    [SerializeField] private Sprite[] _bodies;

    private void Awake()
    {
        ChangeFaceAndBody();
    }

    private void ChangeFaceAndBody()
    {
        GameObject face = transform.Find("Enemy/Body/Face").gameObject;
        face.GetComponent<SpriteRenderer>().sprite = _faces[Random.Range(0, _faces.Length)];

        GameObject body = transform.Find("Enemy/Body").gameObject;
        body.GetComponent<SpriteRenderer>().sprite = _bodies[Random.Range(0, _bodies.Length)];
    }
}